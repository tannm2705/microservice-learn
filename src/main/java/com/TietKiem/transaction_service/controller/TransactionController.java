package com.TietKiem.transaction_service.controller;

import com.TietKiem.transaction_service.model.TransactionModel;
import com.TietKiem.transaction_service.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;


@RestController
@RequestMapping("api/transaction")
@RequiredArgsConstructor
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "GetAllTransaction")
    public List<TransactionModel> getAll() {
        return transactionService.GetAll();
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "CreateTransaction")
    public TransactionModel create(String cifNo, String bankAcct, String savingAcct, String type, BigDecimal amount) {
        TransactionModel model = new TransactionModel();
        model.amount = amount;
        model.type = type;
        model.bank_account_no = bankAcct;
        model.saving_account_no = savingAcct;
        model.customer_cif_no = cifNo;
        int i =0;
        model.id = (long) i;

        return transactionService.cteateTransaction(model);
    }
}

