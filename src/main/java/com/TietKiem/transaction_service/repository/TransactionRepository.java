package com.TietKiem.transaction_service.repository;

import com.TietKiem.transaction_service.model.TransactionModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<TransactionModel, Long> {

}