package com.TietKiem.transaction_service.service;

import com.TietKiem.transaction_service.model.TransactionModel;
import com.TietKiem.transaction_service.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor // create a constructor automatically based on the params during compile time
@Transactional
public class TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;
    public List<TransactionModel> GetAll() {
        return transactionRepository.findAll();
    }

    public TransactionModel cteateTransaction(TransactionModel model) {
        return transactionRepository.save(model);
    }

    public TransactionModel updateTransaction(TransactionModel model) {
        var item = transactionRepository.findById(model.id);

        item.get().amount = model.amount;
        item.get().bank_account_no = model.bank_account_no;
        item.get().type = model.type;
        item.get().saving_account_no  = model.saving_account_no;

        return transactionRepository.save(item.get());
    }

    public String deleteTransaction(Long id) {
        transactionRepository.deleteById(id);
        return "Success";
    }
    public String createSavingAcc(String cifNo, String bankAcctNo, String savingProductType, String savingDate) {
        String savingAcctNo = "";
        return savingAcctNo;
    }

    public Optional<TransactionModel> getDetail(Long id) {
        return transactionRepository.findById(id);
    }

    public TransactionModel getAllWithCifNo(String cifNo, String bankAcc, String savingAcc) {
        return null;
    }
}
