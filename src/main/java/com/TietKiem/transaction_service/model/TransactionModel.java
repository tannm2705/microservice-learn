package com.TietKiem.transaction_service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.java.Log;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "transaction")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionModel {
        public String customer_cif_no;
        public String bank_account_no; //
        public String saving_account_no;
        public String type;
        public BigDecimal amount;
        @Id
        public Long id;
        public Long getId() {
                return id;
        }
        public void setId(Long id) {
                this.id = id;
        }
}
